#!/usr/bin/perl
# filter of help2man output
use strict;
use warnings;
use Dpkg::Changelog::Parse;

# parse changelog for version
my $version;
my %opt;
$opt{file} = "debian/changelog";
my @fields = changelog_parse(%opt);
foreach my $f (@fields) {
    $version = $f->{Version} if exists $f->{Version};
    #strip debian revision
    $version =~ s/(.*)-1$/$1/;
}

while ( <STDIN> ) {
    #multiline regex
    undef $/;
    # remove indent and "boldify" the name
    s/(\.SH\sUSAGE\n*)(\.IP\n|\s|\.TP\n)([\w\d\\-]*)/$1\\fB$3\\fR/xms;
    # remove colon from the name
    s/(\.TH\s([\w\d-]*)):/$1/gmxs;
    # substitude unrecognized version string with the version
    s/([\w\d]*)\:\sunrecognized\soption\s\'--version\'/$1 $version/g;
    s/([\w\d]*)\:\sunrecognized\soption\s\'\\-\\-version\'/ $version/g;
    # there are no invalid options
    s/([\w\d]*)\:\sinvalid\soption\s--\s\'-\'/$1 $version/g;
    # strip full path
    s/\\fI\\,\/.*\/bin(\/utilities)?\///gmxs;
    # strip the header
    s/\.SH\s(DESCRIPTION|SYNOPSIS).*?(Usage:\"?\n(\.IP|\.PP))/.SH USAGE\n\.PP/gmxs;
    # tidy up help2man removeDup output:
    s/Repeated\sRead\sRemoval.*?accessible:\s\'\'//gxms;
    s/(.SH\sNAME\n[\w\d-]*):/$1/gxms;
    # remove "./" from examples
    s/(\\\&\.\/)([\w\d-]*)/$2/gxms;
    s/removeDup\\\\fR\/\\fP/removeDup\\fR/gmxs;
    # massaging for coverageCount which displays help twice
    s/(coverageCount\ Version.*?\n)(.*?)(coverageCount\ Version.*?\n\.IP)/.PP/xms;
    # boldofy "usage" for coverageCount
    s/(\.PP\n)(Usage\n\.IP)(\ncoverageCount)/$1.SH USAGE\n.PP $3/xms;
    s/For\ more\ information\ about\ these\ arguments.*//xms;
	# remove duplicated parts
	s/Usage:\n\.IP.*//xms;
	# remove duplicated output of sublong
	s/(\#\#\sMandatory\sarguments\:).*(\g{1})/$2/gxms;
    print;
}
